#### HỌC LÀM WEBSITE PHẢI BẮT ĐẦU TỪ ĐÂU ?

**Hiện nay, có rất nhiều ngôn ngữ lập trình được áp dụng trong việc xây dựng website.**

**Nhưng căn bản chỉ gồm năm ngôn ngữ chính, đó là: HTML, CSS, JavaScript, MySQL, PHP**

**HTML?**

- HTML là ngôn ngữ đầu tiên mà một nhà thiết kế web phải học
- HTML dùng để "xây dựng nội dung" bên trong trang web

**CSS?**

- CSS là ngôn ngữ thứ hai mà một nhà thiết kế web phải học
- CSS dùng để "định dạng" cho các nội dung bên trong trang web

**JavaScript?**

- JavaScript là ngôn ngữ thứ ba mà một nhà thiết kế web phải học.
- JavaScript dùng để xây dựng chức năng phía người dùng

kidstore code